FROM rust:latest

RUN apt-get update -y
RUN apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    musl \
    musl-dev \
    musl-tools \
    software-properties-common -y 

RUN install -m 0755 -d /etc/apt/keyrings

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg \
&& chmod a+r /etc/apt/keyrings/docker.gpg

RUN echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN apt-get update -y && \
    apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

RUN apt-get clean

RUN rustup default nightly

RUN rustup target add x86_64-unknown-linux-musl --toolchain=nightly

RUN rustup component add clippy rustfmt

RUN cargo +nightly install --git https://github.com/romac/cargo-build-deps.git

